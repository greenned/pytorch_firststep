import torch
from torch import nn, optim
from torch.utils.data import (Dataset, DataLoader, TensorDataset)
from tqdm import tqdm 

from torchvision.datasets import FashionMNIST
from torchvision import transforms

# 00. 데이터 다운로드 및 로드
DATA_PATH = "./data"
FOLDER_NAME = "FashionMNIST"
batch_size = 128
DEVICE = "cpus"

fashion_mnisft_train = FashionMNIST("{}/{}".format(DATA_PATH, FOLDER_NAME), train=True,
                                    download=True, transform=transforms.ToTensor())

fashion_mnisft_test = FashionMNIST("{}/{}".format(DATA_PATH, FOLDER_NAME), train=False,
                                    download=True, transform=transforms.ToTensor())

# 배치사이즈 128인 dataloader 생성
train_loader = DataLoader(fashion_mnisft_train, batch_size=batch_size, shuffle=True)
test_loader = DataLoader(fashion_mnisft_test, batch_size=batch_size, shuffle=False)

# 01. 모형생성
# CNN출력을 MLP에 전달하기 위해 Flatten
class FlattenLayer(nn.Module):
    def forward(self, x):
        sizes = x.size()
        return x.view(sizes[0], -1)

# 5x5 커널사용, 32개 -> 64개 채널로 작성
# BatchNorm2d, Dropout2d는 이미지용
# 마지막에 FlattenLayer적용
conv_net = nn.Sequential(
    nn.Conv2d(1, 32, 5),
    nn.MaxPool2d(2),
    nn.ReLU(),
    nn.BatchNorm2d(32),
    nn.Dropout2d(0.25),
    nn.Conv2d(32, 64, 5),
    nn.MaxPool2d(2),
    nn.ReLU(),
    nn.BatchNorm2d(64),
    nn.Dropout2d(0.25),
    FlattenLayer()
)

# 합성곱에 의해 최정적으로 이미지 크기가 어떤지를 더미 데이터를 통해 확인
test_input = torch.ones(1, 1, 28, 28)
conv_output_size = conv_net(test_input).size()[-1]

# 2층 MLP
mlp = nn.Sequential(
    nn.Linear(conv_output_size, 200),
    nn.ReLU(),
    nn.BatchNorm1d(200),
    nn.Dropout(0.25),
    nn.Linear(200, 100)
)

# 최종 CNN
net = nn.Sequential(
    conv_net,
    mlp
)

# 02. 평가와 훈련용 함수 작성
# 평가용 헬퍼 함수
def eval_net(net, data_loader, device=DEVICE):
    net.eval()
    ys = list()
    ypreds = list()
    for x, y in data_loader:
        x = x.to(device)
        y = y.to(device)
        # 평가용에서는 forward만 하면 되니 자동 미분 불필요.
        with torch.no_grad():
            _, y_pred = net(x).max(1)
        ys.append(y)
        ypreds.append(y_pred)

    ys = torch.cat(ys)
    ypreds = torch.cat(ypreds)

    acc = (ys == ypreds).float().sum() / len(ys)
    return acc.item()

# 트레이닝용 함수
def train_net(net, train_loader, test_loader, optimizer_cls=optim.Adam,
                loss_fn=nn.CrossEntropyLoss(),
                n_iter=10, device="cpu", debug=True):
    train_losses = []
    train_acc = []
    val_acc = []
    optimizer = optimizer_cls(net.parameters())

    for epoch in tqdm(range(n_iter)):
        running_loss = 0.0
        net.train()
        n=0
        n_acc = 0
        for i, (xx, yy) in tqdm(enumerate(train_loader), total=len(train_loader)):
            xx = xx.to(device)
            yy = yy.to(device)
            if debug: print("xx : {} \n yy : {}".format(xx, yy))
            h = net(xx)
            loss = loss_fn(h, yy)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            n += len(xx)
            _, y_pred = h.max(1)
            if debug: print("y_pred : {}".format(y_pred))
            n_acc += (yy == y_pred).float().sum().item()
        # running_loss의 평균값 append
        train_losses.append(running_loss / i)
        train_acc.append(n_acc / n)
        val_acc.append(eval_net(net, test_loader, device))
        print(epoch, train_losses[-1], train_acc[-1], val_acc[-1], flush=True)

net.to("cuda:0")

train_net(net, train_loader, test_loader, n_iter=20, device="cuda:0")

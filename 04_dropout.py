import torch
from torch import nn, optim
from sklearn.model_selection import train_test_split
from tqdm import tqdm
from torch.utils.data import TensorDataset, DataLoader
from sklearn.datasets import load_digits
from matplotlib import pyplot as plt

# 주피터에서 실행시 시각화를 위해 아래 주석제거
#%matplotlib inline

# 01. sklearn 패키지의 데이터 로드 및 toTensor
digits = load_digits()
X = digits.data
y = digits.target

# 02. train_set, test_set 분리
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.3)
X_train = torch.tensor(X_train, dtype=torch.float32)
y_train = torch.tensor(y_train, dtype=torch.int64)
X_test = torch.tensor(X_test, dtype=torch.float32)
y_test = torch.tensor(y_test, dtype=torch.int64)

# 03. 모형 지정
# Linear -> ReLU -> Dropout -> Linear -> ReLU -> Dropout -> Linear -> ReLU -> Dropout -> Linear -> ReLU -> Dropout -> Linear
# 배치값 정규화 : nn.BatchNorm1d
k = 100
net = nn.Sequential(
    nn.Linear(64, k),
    nn.ReLU(),
    nn.Dropout(0.5),
    # nn.BatchNorm1d(k),
    nn.Linear(k, k),
    nn.ReLU(),
    nn.Dropout(0.5),
    # nn.BatchNorm1d(k),
    nn.Linear(k, k),
    nn.ReLU(),
    nn.Dropout(0.5),
    # nn.BatchNorm1d(k),
    nn.Linear(k, k),
    nn.ReLU(),
    nn.Dropout(0.5),
    # nn.BatchNorm1d(k),
    nn.Linear(k, 10)
)

# 04. 손실함수 지정
loss_fn = nn.CrossEntropyLoss()

# 05. 최적화 함수 지정
optimizer = optim.Adam(net.parameters())

# 06. DataLoader 생성
ds = TensorDataset(X_train, y_train)
loader = DataLoader(ds, batch_size=32, shuffle=True)

# 07. 모델학습
train_lossess = list()
test_lossess = list()
for epoch in tqdm(range(100)):
    running_loss = 0
    for i, (xx, yy) in enumerate(loader):
        y_pred = net(xx)
        loss = loss_fn(y_pred, yy)
        optimizer.zero_grad()
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
    # 검증 데이터의 손실 함수 계산
    train_lossess.append(loss / i)
    # 신경망을 평가모드로 설정
    net.eval()
    y_pred = net(X_test)
    test_loss = loss_fn(y_pred, y_test)
    test_lossess.append(test_loss.item())

# 08. 손실 시각화
plt.plot(train_lossess, label="train")
plt.plot(test_lossess, label="test")
plt.legend()
plt.show()
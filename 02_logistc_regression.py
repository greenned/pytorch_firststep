import torch
from torch import nn, optim
from sklearn.datasets import load_digits
from tqdm import tqdm
from matplotlib import pyplot as plt

# 01. sklearn 패키지의 데이터 로드 및 toTensor
digits = load_digits()
X = digits.data
y = digits.target
X = torch.tensor(X, dtype=torch.float32)
y = torch.tensor(y, dtype=torch.int64)

print("X_size : ", X.size())

# 02. 선형회귀 모형지정 input_features = X.size()[1], output_features = 10 (분류값 10개)
net = nn.Linear(X.size()[1], 10)

# 03. 손실함수 CrossEntorpy로 지정
loss_fn = nn.CrossEntropyLoss()

# 04. 최적화 로직은 Stochastic Gradient Descent, learning_reate = 0.1
optimizer = optim.SGD(net.parameters(), lr=0.01)

# 05. 모델 트레이닝
# loss값 기록 리스트
losses = list()

for epoch in tqdm(range(100)):
    # 미분결과 경사값 초기화
    optimizer.zero_grad()
        # 선형모델로 y값 예측
    y_pred = net(X)
        # loss계산
    loss = loss_fn(y_pred, y)
        # loss의 w를 사용하여 미분 계산
    loss.backward()
        # 경사도 갱신
    optimizer.step()
        # 경사도 값 append
    losses.append(loss)

# 06. loss 감소 시각화
plt.plot(losses)

# 07. 정답률 계싼
# torch.max는 최댓값과 그 위치도 반환
_, y_pred = torch.max(net(X), 1)
# 정답률 계산
print((y_pred == y).sum().item() / len(y))
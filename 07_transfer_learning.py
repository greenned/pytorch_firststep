import os
from torchvision.datasets import ImageFolder
from torch.utils.data import DataLoader
from torchvision import transforms
from torchvision import models
from torch import nn, optim
import torch
from tqdm import tqdm

# 00. 이미지 불러오기
# 트레이닝셋은 랜덤크롭 224x224 픽셀
# 테스트셋은 센터크롭 224x224 픽셀
FOLDER_PATH = "./data"
train_imgs = ImageFolder(os.path.join(FOLDER_PATH, "taco_and_burrito/train/"),
                        transform=transforms.Compose([
                            transforms.RandomCrop(224),
                            transforms.ToTensor()
                        ]))
test_imgs = ImageFolder(os.path.join(FOLDER_PATH, "taco_and_burrito/test/"),
                        transform=transforms.Compose([
                            transforms.CenterCrop(224),
                            transforms.ToTensor()
                        ]))

# 데이터로더 작성
BATCH_SIZE = 32
train_loader = DataLoader(train_imgs, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(test_imgs, batch_size=BATCH_SIZE, shuffle=False)

# 01. 모형 만들기
# 사전에 학습된 모형 resnet18 불러오기
net = models.resnet18(pretrained=True)

# 모든 파라미터를 미분 대상에서 제외
for p in net.parameters():
    p.requires_grad=False

# 마지막 선형 계층을 변경
fc_input_dim = net.fc.in_features
net.fc = nn.Linear(fc_input_dim, 2)

def eval_net(net, data_loader, device="cpu"):
    net.eval()
    ys = list()
    ypreds = list()
    for x, y in data_loader:
        x = x.to(device)
        y = y.to(device)
        with torch.no_grad():
            _, y_pred = net(x).max(1)
        ys.append(y)
        ypreds.append(y_pred)
    
    ys = torch.cat(ys)
    ypreds = torch.cat(ypreds)
    acc = (ys == ypreds).float().sum() / len(ys)
    return acc.item()

def train_net(net, train_loader, test_loader, only_fc=True, optimizer_cls=optim.Adam,
                loss_fn=nn.CrossEntropyLoss(), n_iter=10, device="cpu"):
    train_losses = list()
    train_acc = list()
    val_acc = list()
    if only_fc:
        # 마지막 선형 계층의 파라미터만 전달
        optimizer = optimizer_cls(net.fc.parameters())
    else:
        optimizer = optimizer_cls(net.parameters())
    for epoch in tqdm(range(n_iter), total=n_iter):
        running_loss = 0.0
        net.train()
        n = 0
        n_acc = 0.0
        for i, (xx, yy) in enumerate(train_loader):
            xx = xx.to(device)
            yy = yy.to(device)
            h = net(xx)
            loss = loss_fn(h, yy)
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            running_loss += loss.item()
            n += len(xx)
            _, y_pred = h.max(1)
            n_acc += (yy==y_pred).float().sum().item()
        train_losses.append(running_loss / i)
        train_acc.append(n_acc / n)

        val_acc.append(eval_net(net, test_loader, device))
        print(epoch, train_losses[-1], train_acc[-1], val_acc[-1],flush=True)

net.to("cuda:0")
train_net(net, train_loader, test_loader, n_iter=20, device="cuda:0")



import torch
from torch import nn, optim
from tqmd import tqdm

# nn, optim을 이용한 기본선형회귀.

# 최종 예측값과 비교하기 위해 참인값을 사전에 지정 
w_true = torch.Tensor([1,2,3])

# X, y 값 지정
X = torch.cat([torch.ones(100, 1), torch.randn(100, 2)], 1)
y = torch.mv(X, w_true) + torch.randn(100) * 0.5

# 선형회귀 모형 지정
# input값 3개, output값 1개, bias 없음
net = nn.Linear(in_features=3, out_features=1, bias=False)

# 최적화 로직은 Stochastic Gradient Descent
optimizer = optim.SGD(net.parameters(), lr=0.1)

# Loss 함수는 Mean Square Error 
loss_fn = nn.MSELoss()

# loss값 기록 리스트
losses = list()

for epoc in tqdm(range(100)):
    # 미분결과 경사값 초기화
    optimizer.zero_grad()
    # 선형모델로 y값 예측
    y_pred = net(X)
    # loss계산
    loss = loss_fn(y_pred.view_as(y),y)
    # loss의 w를 사용하여 미분 계산
    loss.backward()
    # 경사도 갱신
    optimizer.step()
    # 경사도 값 append
    losses.append(loss.item())


'''
# nn, optime을 생략하고 만든 선형회귀

import torch
from tqdm import tqdm
from matplotlib import pyplot as plt
device = "cuda:0"
#%matplotlib inline


w_true = torch.Tensor([1,2,3])

X = torch.cat([torch.ones(100, 1), torch.randn(100, 2)], 1)

y = torch.mv(X, w_true) + torch.randn(100) * 0.5
print(y)

w = torch.randn(3, requires_grad=True)
gamma = 0.1

losses = list()

for epoc in tqdm(range(100000)):
    w.grad = None
    y_pred = torch.mv(X, w)
    loss = torch.mean((y-y_pred)**2)
    loss.backward()
    w.data = w.data - gamma * w.grad.data
    losses.append(loss.item())

print(min(losses))

plt.plot(losses)
print(w)



#print(torch.randn(100, 2))
'''
import torch
from torch import nn, optim
from sklearn.datasets import load_digits
from tqdm import tqdm
from matplotlib import pyplot as plt
from torch.utils.data import TensorDataset, DataLoader

# 01. sklearn 패키지의 데이터 로드 및 toTensor
digits = load_digits()
X = digits.data
y = digits.target
X = torch.tensor(X, dtype=torch.float32)
y = torch.tensor(y, dtype=torch.int64)

# 02. MLP 모형 지정 Linear -> ReLU -> Linear -> ReLU -> Linear
net = nn.Sequential(
    nn.Linear(64, 32),
    nn.ReLU(),
    nn.Linear(32, 16),
    nn.ReLU(),
    nn.Linear(16, 10)
)

# 03. GPU할당 배치를 위한 데이터 병합
# CUDA 사용불가능시 아래 3줄 코드 주석처리
X = X.to("cuda:0")
y = y.to("cuda:0")
net.to("cuda:0")

# TensorDataset : X, y값을 병합하는 클래스
ds = TensorDataset(X, y)
# DataLoader : 배치사이즈 64와 shuffle하는 데이터로더
loader = DataLoader(ds, batch_size=64, shuffle=True)

# 04. 손실함수 CrossEntorpy로 지정
loss_fn = nn.CrossEntropyLoss()
# 05. 최적화 함수는 Adam
optimizer = optim.Adam(net.parameters())

# 06. 모델트레이닝
losses = list()
for epoch in tqdm(range(1000)):
    running_loss = 0.0
    for xx, yy in loader:
        optimizer.zero_grad()
        y_pred = net(xx)
        loss = loss_fn(y_pred, yy)
        loss.backward()
        optimizer.step()
        running_loss += loss.item()
        losses.append(running_loss)
